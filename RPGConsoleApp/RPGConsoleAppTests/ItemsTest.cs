﻿using RPGConsoleApp;
using RPGConsoleApp.Attributes;
using RPGConsoleApp.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPGConsoleAppTests {
    public class ItemsTest {
        #region Assign high level weapon to Warrior
        [Fact]
        public void Warrior_EquipWeapon_EquippingHighLevelWeapon_ShouldThrowInvalidWeaponException() {
            // Arrange
            Warrior testWarrior = new Warrior("testWarrior");
            Weapon testAxe = new Weapon() {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = Slot.WEAPON,
                WeaponType = WeaponType.WANDS,
                WeaponAttribute = new WeaponAttribute() { Damage = 7, AttackSpeed = 1.1 }
            };
            // Act + Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeaponToCharater(testAxe));
        }

        #endregion
        #region Assign high level armour to Warrior
        [Fact]
        public void Warrior_EquippingHighLevelArmour_ShouldThrowInvalidWeaponException() {
            // Arrange
            Warrior testWarrior = new Warrior("testWarrior");
            Armour testPlateBody = new Armour() {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = Slot.BODY,
                ArmourType = ArmourType.PLATE,
                PrimaryAttribute = new BasePrimaryAttribute() { Vitality = 2, Strength = 1 }
            };
            // Act + Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmourToCharater(testPlateBody));
        }
        #endregion
        #region Assign wrong equipment to Warrior

        [Fact]
        public void Warrior_EquipWrongWeaponType_ShouldThrowInvalidWeaponException() {
            // Arrange
            Warrior testWarrior = new Warrior("testWarrior");
            Weapon testBow = new Weapon() {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON,
                WeaponType = WeaponType.BOWS,
                WeaponAttribute = new WeaponAttribute() { Damage = 12, AttackSpeed = 0.8 }
            };
            // Act + Assert
            Assert.Throws<InvalidWeaponException>(() => testWarrior.EquipWeaponToCharater(testBow));
        }
        #endregion
        #region Assign wrong armour to Warrior
        [Fact]
        public void Warrior_EquipWrongArmourType_ShouldThrowInvalidArmorException() {
            // Arrange
            Warrior testWarrior = new Warrior("testWarrior");
            Armour testClothHead = new Armour() {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = Slot.HEAD,
                ArmourType = ArmourType.CLOTH,
                PrimaryAttribute = new BasePrimaryAttribute() { Vitality = 1, Intelligence = 5 }
            };
            // Act + Assert
            Assert.Throws<InvalidArmorException>(() => testWarrior.EquipArmourToCharater(testClothHead));
        }
        #endregion
        #region Verify success message after assigning equipment
        [Fact]
        public void Warrior_EquipValidWeapon_ShouldReturnSuccessMessage() {
            // Arrange
            string expected = "Success! weapon equipped";
            Warrior testWarrior = new Warrior("testWarrior");
            Weapon testAxe = new Weapon() {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON,
                WeaponType = WeaponType.AXES,
                WeaponAttribute = new WeaponAttribute() { Damage = 7, AttackSpeed = 1.1 }
            };
            // Act
            string actual = testWarrior.EquipWeaponToCharater(testAxe);
            // Assert
            Assert.Equal(expected, actual);
        }
        #endregion
        #region
        [Fact]
        public void Warrior_EquipValidArmor_ShouldReturnSuccessMessage() {
            // Arrange
            string expected = "Success! armour equipped";
            Warrior testWarrior = new Warrior("testWarrior");
            Armour testPlateBody = new Armour() {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.BODY,
                ArmourType = ArmourType.PLATE,
                PrimaryAttribute = new BasePrimaryAttribute() { Vitality = 2, Strength = 1 }
            };
            // Act
            string actual = testWarrior.EquipArmourToCharater(testPlateBody);
            // Assert
            Assert.Equal(expected, actual);
        }
        #endregion
    }
}