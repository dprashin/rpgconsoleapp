using RPGConsoleApp;
using RPGConsoleApp.Characters;
using System;
using Xunit;

namespace RPGConsoleAppTests {
    public class CharacterUnitTest {
        [Fact]
        public void Character_CreateCharacter_CharacterShouldHaveLevel1() {
            //Arrange
            Mage character = new Mage("Test");
            int expected = 1;
            
            //Act
            int actual = character.Level;

            //Assert 
            Assert.Equal(expected, actual);
        }

        [Fact]

        public void Character_LevelUpWithValidNumber() {
            //Arrange
            Ranger character = new Ranger("Test");
            character.LevelUp(1);

            //Act
            int actual = character.Level;
            int expected = 2;

            //Assert 
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData (-1)]
        [InlineData (0)]
        public void Character_LevelUpWithNonValidNumbers(int level) {
            //Arrange
            Mage character = new Mage("Test");
            character.Level = level;

            //Act
            int actual = character.Level;

            //Assert 
            Assert.Throws<ArgumentException>(() => character.LevelUp(level));
        }

    }
}
