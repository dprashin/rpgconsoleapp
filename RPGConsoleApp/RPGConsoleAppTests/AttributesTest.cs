﻿using RPGConsoleApp;
using RPGConsoleApp.Attributes;
using RPGConsoleApp.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RPGConsoleAppTests {
    public class AttributesTest {
        #region Mage attributes test
        [Fact]
        public void Mage_VerifyBasePrimaryAttribures() {
            //Arrange
            BasePrimaryAttribute expected = new BasePrimaryAttribute {
                Dexterity = 1,
                Intelligence = 8,
                Strength = 1,
                Vitality = 5
            };
            Mage mage = new Mage("Mage");
            BasePrimaryAttribute actual = mage.PrimaryAttribute;
            // Assert
            Assert.True(expected.Equals(actual));
        }
        #endregion
        #region Warrier attributes test
        [Fact]
        public void Warrior_VerifyBasePrimaryAttribures() {
            // Arrange

            BasePrimaryAttribute expected = new BasePrimaryAttribute {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
                Vitality = 10,
            };
            // Act
            Warrior warrior = new Warrior("Warrior");
            BasePrimaryAttribute actual = warrior.PrimaryAttribute;
            // Assert
            Assert.True(expected.Equals(actual));
        }
        #endregion

        #region Ranger attributes test
        [Fact]
        public void Ranger_VerifyBasePrimaryAttribures() {
            // Arrange

            BasePrimaryAttribute expected = new BasePrimaryAttribute {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1,
                Vitality = 8
            };
            // Act
            Ranger ranger = new Ranger("Ranger");
            BasePrimaryAttribute actual = ranger.PrimaryAttribute;
            // Assert
            Assert.True(expected.Equals(actual));
        }
        #endregion

        #region Rouge attributes test
        [Fact]
        public void Rouge_VerifyBasePrimaryAttribures() {
            // Arrange

            BasePrimaryAttribute expected = new BasePrimaryAttribute {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
                Vitality = 8
            };
            // Act
            Rouge rouge = new Rouge("Rouge");
            BasePrimaryAttribute actual = rouge.PrimaryAttribute;
            // Assert
            Assert.True(expected.Equals(actual));
        }
        #endregion
        #region Mage character LevelUp by 1
        [Fact]
        public void LevelUp_MageCharacterBy1() {
            // Arrange
            Mage mage = new Mage("Mage");
            BasePrimaryAttribute expected = new BasePrimaryAttribute {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13,
                Vitality = 8
            };
            // Act
            mage.LevelUp(1);
            BasePrimaryAttribute actual = mage.PrimaryAttribute;
            // Assert
            Assert.True(expected.Equals(actual));
        }
        #endregion
        #region Warrior character LevelUp by 1
        [Fact]
        public void LevelUp_WarriorCharacterBy1() {
            // Arrange
            Warrior warrior = new Warrior("War");
            BasePrimaryAttribute expected = new BasePrimaryAttribute {
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2,
                Vitality = 15
            };
            // Act
            warrior.LevelUp(1);
            BasePrimaryAttribute actual = warrior.PrimaryAttribute;
            // Assert
            Assert.True(expected.Equals(actual));
        }
        #endregion

        #region Rouge character LevelUp by 1
        [Fact]
        public void LevelUp_RougeCharacterBy1() {
            // Arrange
            Rouge rouge = new Rouge("Rouge");
            BasePrimaryAttribute expected = new BasePrimaryAttribute {
                Strength = 3,
                Dexterity = 10,
                Intelligence = 2,
                Vitality = 11
            };
            // Act
            rouge.LevelUp(1);
            BasePrimaryAttribute actual = rouge.PrimaryAttribute;
            // Assert
            Assert.True(expected.Equals(actual));
        }
        #endregion


        #region Ranger character LevelUp by 1
        [Fact]
        public void LevelUp_RangerCharacterBy1() {
            // Arrange
            Ranger ranger = new Ranger("Ranger");
            BasePrimaryAttribute expected = new BasePrimaryAttribute {
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2,
                Vitality = 10
            };
            // Act
            ranger.LevelUp(1);
            BasePrimaryAttribute actual = ranger.PrimaryAttribute;
            // Assert
            Assert.True(expected.Equals(actual));
        }
        #endregion

        #region
        [Fact]
        public void LevelUp_WarriorCharacterBy1_VerifySecondaryAttributes() {
            // Arrange
            Warrior warrior = new("Warrior");
            SecondaryAttribute expectedSecondaryAttributes = new() {
                Health = 150,
                ArmorRating = 12,
                ElementalResistance = 2
            };
            // Act
            warrior.LevelUp(1);
            SecondaryAttribute actualSecondaryAttributes = warrior.SecondaryAttribute;
            // Assert
            Assert.True(expectedSecondaryAttributes.Equals(actualSecondaryAttributes));
        }
        #endregion
    }
}