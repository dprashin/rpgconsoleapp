﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp.Attributes {
    /// <summary>
    /// A BasePrimaryAttribute class.
    /// </summary>
    public class BasePrimaryAttribute {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
        public int Vitality { get; set; }
        /// <summary>
        /// Compares two bojects by its base primary attributes.
        /// </summary>
        /// <param name="obj">An object.</param>
        /// <returns>A boolean value - True / False.</returns>
        public override bool Equals(object obj) {
            return obj is BasePrimaryAttribute attributes &&
                   Strength == attributes.Strength &&
                   Dexterity == attributes.Dexterity &&
                   Intelligence == attributes.Intelligence &&
                   Vitality == attributes.Vitality;
        }
        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>Integer hash value.</returns>
        public override int GetHashCode() {
            return HashCode.Combine(Strength, Dexterity, Intelligence, Vitality);
        }
        /// <summary>
        /// Sums base primary attributes.
        /// </summary>
        /// <param name="attrLhs">A BasePrimaryAttribute object.</param>
        /// <param name="attRhs">A BasePrimaryAttribute object.</param>
        /// <returns></returns>
        public static BasePrimaryAttribute operator +(BasePrimaryAttribute attrLhs, BasePrimaryAttribute attRhs) {
            return new BasePrimaryAttribute {
                Strength = attrLhs.Strength + attRhs.Strength,
                Dexterity = attrLhs.Dexterity + attRhs.Dexterity,
                Intelligence = attrLhs.Intelligence + attRhs.Intelligence,
                Vitality = attrLhs.Vitality + attRhs.Vitality
            };
        }


    }
}
