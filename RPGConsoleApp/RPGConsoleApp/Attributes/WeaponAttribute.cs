﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp.Items {
    /// <summary>
    /// A weapon attribute class.
    /// </summary>
    public class WeaponAttribute {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
    }
}
