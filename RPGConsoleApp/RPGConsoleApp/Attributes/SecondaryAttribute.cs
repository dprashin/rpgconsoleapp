﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp.Attributes {
    //Secondary Attributes of a character
    public class SecondaryAttribute {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }
        public double DamagePerSecond { get; set; }
        /// <summary>
        /// Compares two bojects by its secondary attributes.
        /// </summary>
        /// <param name="obj">An object.</param>
        /// <returns>A boolean value - True / False.</returns>
        public override bool Equals(object obj) {
            return obj is SecondaryAttribute attributes &&
                   Health == attributes.Health &&
                   ArmorRating == attributes.ArmorRating &&
                   ElementalResistance == attributes.ElementalResistance;
        }
        /// <summary>
        /// Serves as the default hash function.
        /// </summary>
        /// <returns>Integer hash value.</returns>
        public override int GetHashCode() {
            return HashCode.Combine(Health, ArmorRating, ElementalResistance);
        }
        /// <summary>
        /// Sums secondary attributes.
        /// </summary>
        /// <param name="attrLhs">A SecondaryAttribute object.</param>
        /// <param name="attRhs">A SecondaryAttribute object.</param>
        /// <returns></returns>
        public static SecondaryAttribute operator +(SecondaryAttribute attrLhs, SecondaryAttribute attRhs) {
            return new SecondaryAttribute {
                Health = attrLhs.Health + attRhs.Health,
                ArmorRating = attrLhs.ArmorRating + attRhs.ArmorRating,
                ElementalResistance = attrLhs.ElementalResistance + attRhs.ElementalResistance,
            };
        }
    }
}
