﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp.Items {
    /// <summary>
    /// An InvalidWeaponException class.
    /// </summary>
    public class InvalidWeaponException : Exception {
        /// <summary>
        /// Overrides the default expeption message and returns a new message.
        /// </summary>
        public override string Message {
            get {
                return "Wrong character type or level too low to use this weapon";
            }
        }

    }
}
