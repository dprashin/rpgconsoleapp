﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp.Items {
    /// <summary>
    /// Slot types.
    /// </summary>
    public enum Slot {
        HEAD,
        BODY,
        LEGS,
        WEAPON
    }
    public abstract class Item {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public Slot ItemSlot { get; set; }
        /// <summary>
        /// Constructor for creating items. Initializes item level to 1. 
        /// </summary>
        public Item() {
            this.ItemLevel = 1;
        }
    }
}
