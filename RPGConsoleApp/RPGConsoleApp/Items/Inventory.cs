﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp.Items {
    /// <summary>
    /// An Inventory class.
    /// </summary>
    public class Inventory {
        public Dictionary<Slot, Armour> ItemsInventory { get; set; }
        /// <summary>
        /// Constructor for creating inventory. 
        /// </summary>
        public Inventory() {
            ItemsInventory = new Dictionary<Slot, Armour>();
        }
        /// <summary>
        /// Adds an armour to an inventory. 
        /// </summary>
        /// <param name="armour">An armour object.</param>
        public void AddItemInInventory(Armour armour) {
            ItemsInventory.Add(armour.ItemSlot, armour);
        }
    }
}
