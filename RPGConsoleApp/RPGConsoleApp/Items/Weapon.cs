﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp.Items {
    /// <summary>
    /// Weapon types.
    /// </summary>
    public enum WeaponType {
        AXES,
        BOWS,
        DAGGERS,
        HAMMERS,
        STAFFS,
        SWRODS,
        WANDS
    }
    /// <summary>
    /// A weapon class.
    /// </summary>
    public class Weapon : Item {
        public WeaponType WeaponType { get; set; }
        public WeaponAttribute WeaponAttribute { get; set; }
        /// <summary>
        /// Constructor for creating weapons.
        /// </summary>
        public Weapon() {
            WeaponAttribute = new WeaponAttribute();
        }
        /// <summary>
        /// Calculates damage per second.
        /// </summary>
        /// <returns></returns>
        public double CalculateDPS() {
            return WeaponAttribute.AttackSpeed * WeaponAttribute.Damage;
        } 

    }
}
