﻿using RPGConsoleApp.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp.Items {
    /// <summary>
    /// Armour types.
    /// </summary>
    public enum ArmourType {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }
    /// <summary>
    /// An Armour class.
    /// </summary>
    public class Armour : Item {
        public ArmourType ArmourType { get; set; }
        public BasePrimaryAttribute PrimaryAttribute { get; set; }
    }
}
