﻿using RPGConsoleApp.Attributes;
using RPGConsoleApp.Characters;
using RPGConsoleApp.Items;
using System;

namespace RPGConsoleApp {
    class Program {
        static void Main(string[] args) {
            //Mage
            Mage mage = new Mage("Character 1");
            Weapon testAxe = new Weapon() {
                ItemName = "Axe",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON,
                WeaponType = WeaponType.STAFFS,
                WeaponAttribute = new WeaponAttribute() { Damage = 7, AttackSpeed = 1.1 }
            };

            Armour testPlateBody = new Armour() {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.BODY,
                ArmourType = ArmourType.CLOTH,
                PrimaryAttribute = new BasePrimaryAttribute() { Vitality = 2, Strength = 1 }
            };

            mage.LevelUp(2);
            mage.EquipWeaponToCharater(testAxe);
            mage.EquipArmourToCharater(testPlateBody);
            mage.DisplayCharacterStats();

            //Rogue
            Rouge rouge = new Rouge("Character 2");
            Weapon testBows = new Weapon() {
                ItemName = "Bows",
                ItemLevel = 1,
                ItemSlot = Slot.WEAPON,
                WeaponType = WeaponType.DAGGERS,
                WeaponAttribute = new WeaponAttribute() { Damage = 7, AttackSpeed = 2.1 }
            };

            Armour testLeatherBody = new Armour() {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = Slot.BODY,
                ArmourType = ArmourType.LEATHER,
                PrimaryAttribute = new BasePrimaryAttribute() { Vitality = 3, Strength = 1 }
            };

            rouge.LevelUp(1);
            rouge.EquipWeaponToCharater(testBows);
            rouge.EquipArmourToCharater(testLeatherBody);
            rouge.DisplayCharacterStats();


            Console.WriteLine();
        }
    }
}
