﻿using RPGConsoleApp.Attributes;
using RPGConsoleApp.Items;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp {
    /// <summary>
    /// Enum for storing character types.
    /// </summary>
    public enum CharatcterType {
        MAGE,
        RANGER,
        ROUGE,
        WARRIOR
    }
    /// <summary>
    /// An abstract class used in while creating concrete/real characters.
    /// </summary>
    public abstract class Character {

        public string Name { get; set; }
        public int Level { get; set; }
        public CharatcterType CharatcterType { get; set; }

        public BasePrimaryAttribute PrimaryAttribute { get; set; }

        public BasePrimaryAttribute TotalPrimaryAttributes { get; set; }

        public SecondaryAttribute SecondaryAttribute { get; set; }

        public Weapon weapon;
        public Inventory CharactorInventory { get; set; }

        public Character(string name) {
            Name = name;
            Level = 1;
            PrimaryAttribute = new BasePrimaryAttribute();
            SecondaryAttribute = new SecondaryAttribute();
            TotalPrimaryAttributes = new BasePrimaryAttribute();
            weapon = new Weapon();
            CharactorInventory = new Inventory();
        }
        /// <summary>
        /// Calculates character damage. The calculation is based on weapon attributes and character type. 
        /// </summary>
        public void CalculateDamage() {
            int totalPrimaryAttributes = 0;
            double weaponDamagePerSecond = weapon.CalculateDPS();

            switch (CharatcterType) {
                case CharatcterType.MAGE:
                    totalPrimaryAttributes = TotalPrimaryAttributes.Intelligence;
                    break;
                case CharatcterType.RANGER:
                    totalPrimaryAttributes = TotalPrimaryAttributes.Dexterity;
                    break;
                case CharatcterType.ROUGE:
                    totalPrimaryAttributes = TotalPrimaryAttributes.Dexterity;
                    break;
                case CharatcterType.WARRIOR:
                    totalPrimaryAttributes = TotalPrimaryAttributes.Strength;
                    break;
                default:
                    break;
            }
            SecondaryAttribute.DamagePerSecond = weaponDamagePerSecond * (1 + (totalPrimaryAttributes / 100));
        }
        /// <summary>
        /// Updates/sums primary attributes. The total attributes is calculated
        /// by adding base primary attributes together and primary attributes from armor which are stored in an inventory.
        /// Each time total attributes is calculated secondary attributes and damage are also calculated.
        /// </summary>
        public void UpdateTotalAttributes() {
            BasePrimaryAttribute inventoryPrimaryAttributes = AddAttributesFromInventory();
            TotalPrimaryAttributes = PrimaryAttribute + inventoryPrimaryAttributes;
            CalculateSecondayAttributes();
            CalculateDamage();
        }

        /// <summary>
        /// Sums primary attributes from all armours in inventory. 
        /// </summary>
        /// <returns>A base primary attribute object. </returns>
        private BasePrimaryAttribute AddAttributesFromInventory() {
            BasePrimaryAttribute inventoryPrimaryAttributes = new BasePrimaryAttribute();
            foreach (KeyValuePair<Slot, Armour> item in CharactorInventory.ItemsInventory) {
                Armour tmp;
                CharactorInventory.ItemsInventory.TryGetValue(item.Key, out tmp);
                inventoryPrimaryAttributes += tmp.PrimaryAttribute;
            }
            return inventoryPrimaryAttributes;
        }
        /// <summary>
        /// An abstract method for leveling up a character.
        /// </summary>
        /// <param name="level">Level number </param>
        public abstract void LevelUp(int level);

        /// <summary>
        /// Calculates secondary attributes of a charactor. Calculation is dependent on the total primary attributes i.e. base primary attributes and items primary attributes.
        /// </summary>
        public void CalculateSecondayAttributes() {
            //Each point of vitality increases health by 10
            SecondaryAttribute.Health = TotalPrimaryAttributes.Vitality * 10;
            //Each point of Strength or Dexterity adds one armor rating
            SecondaryAttribute.ArmorRating = TotalPrimaryAttributes.Dexterity + TotalPrimaryAttributes.Strength;
            //Each point of Intelligence adds one elemental resistance
            SecondaryAttribute.ElementalResistance = TotalPrimaryAttributes.Intelligence;
        }
        /// <summary>
        /// An abstract method for equipping a weapon to a character.
        /// </summary>
        /// <param name="weapon">A weapon object.</param>
        /// <returns>Success or failure string message.</returns>
        public abstract string EquipWeaponToCharater(Weapon weapon);
        /// <summary>
        /// An abstract method for equipping an armour to a character.
        /// </summary>
        /// <param name="armour">An armour object.</param>
        /// <returns>Success or failure string message.</returns>
        public abstract string EquipArmourToCharater(Armour armour);

        /// <summary>
        /// Displays stats of a charactor. 
        /// </summary>
        public void DisplayCharacterStats() {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Character Name: {0}", Name).Append("\n");
            sb.AppendFormat("Character Type: {0}", CharatcterType).Append("\n");
            sb.AppendFormat("Character Level: {0}", Level).Append("\n");
            sb.AppendFormat("Vitality Score: {0}", TotalPrimaryAttributes.Vitality).Append("\n");
            sb.AppendFormat("Strength Score: {0}", TotalPrimaryAttributes.Strength).Append("\n");
            sb.AppendFormat("Dexterity Score: {0}", TotalPrimaryAttributes.Dexterity).Append("\n");
            sb.AppendFormat("Intelligence Score: {0}", TotalPrimaryAttributes.Intelligence).Append("\n");
            sb.AppendFormat("Armour Rating Score: {0}", SecondaryAttribute.ArmorRating).Append("\n");
            sb.AppendFormat("Elemental Resistance Score: {0}", SecondaryAttribute.ElementalResistance).Append("\n");
            sb.AppendFormat("Damage score: {0}", SecondaryAttribute.DamagePerSecond).Append("\n");
            sb.AppendFormat("*****************************************").Append("\n");

            Console.WriteLine(sb);
        }
    }
}
