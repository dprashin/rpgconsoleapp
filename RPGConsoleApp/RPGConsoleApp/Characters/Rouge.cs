﻿using RPGConsoleApp.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp {
    /// <summary>
    /// A Ranger charater class.
    /// </summary>
    public class Rouge : Character {
        /// <summary>
        /// Constructor for a Rouge charactor. Also, updates total primary attributes.    
        /// </summary>
        /// <param name="name">A name of a charater</param>
        public Rouge(string name): base(name) {
            base.PrimaryAttribute.Vitality = 8;
            base.PrimaryAttribute.Strength = 2;
            base.PrimaryAttribute.Dexterity = 6;
            base.PrimaryAttribute.Intelligence = 1;
            base.CharatcterType = CharatcterType.ROUGE;

            UpdateTotalAttributes();
        }
        /// <summary>
        /// Equips an armour to a character. 
        /// </summary>
        /// <param name="armour">An Armour object.</param>
        /// <returns>Success or failure string message.</returns>
        public override string EquipArmourToCharater(Armour armour) {
            string result = "";
            if (armour.ItemLevel > Level ||
                armour.ArmourType == ArmourType.CLOTH ||
                armour.ArmourType == ArmourType.PLATE) {
                throw new InvalidArmorException();
            } else {
                CharactorInventory.AddItemInInventory(armour);
                result = "Success! armour equipped";
                UpdateTotalAttributes();
            }
            return result;
        }
        /// <summary>
        /// Equips a weapon to a character. 
        /// </summary>
        /// <param name="weapon">A weapon object.</param>
        /// <returns>Success or failure string message.</returns>
        public override string EquipWeaponToCharater(Weapon weapon) {
            string result = "";
            if (weapon.ItemLevel <= Level && (weapon.WeaponType == WeaponType.DAGGERS || weapon.WeaponType == WeaponType.SWRODS)) {
                this.weapon = weapon;
                result = "Success! weapon equipped";
                CalculateDamage();
            } else {
                throw new InvalidWeaponException();
            }
            return result;
        }
        /// <summary>
        /// Levels up a character.
        /// </summary>
        /// <param name="level">Level number.</param>
        public override void LevelUp(int level) {
            if (level <= 0) {
                throw new ArgumentException("Not a valid level");
            } else {
                Level += level;
                this.PrimaryAttribute.Vitality += (level * 3);
                this.PrimaryAttribute.Strength += (level * 1);
                this.PrimaryAttribute.Dexterity += (level * 4);
                this.PrimaryAttribute.Intelligence += (level * 1);

                UpdateTotalAttributes();
            }
        }
    }
}
