﻿using RPGConsoleApp.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGConsoleApp.Characters {
    /// <summary>
    /// A Mage charater class.
    /// </summary>
    public class Mage : Character {
        /// <summary>
        /// Constructor for a Mage charactor. Also, updates total primary attributes.    
        /// </summary>
        /// <param name="name">A name of a charater</param>
        public Mage(string name): base(name) {
            base.PrimaryAttribute.Vitality = 5;
            base.PrimaryAttribute.Strength = 1;
            base.PrimaryAttribute.Dexterity = 1;
            base.PrimaryAttribute.Intelligence = 8;
            base.CharatcterType = CharatcterType.MAGE;

            UpdateTotalAttributes();
        }
        /// <summary>
        /// Equips an armour to a character. 
        /// </summary>
        /// <param name="armour">An Armour object.</param>
        /// <returns>Success or failure string message.</returns>
        public override string EquipArmourToCharater(Armour armour) {
            string result = "";
            if(armour.ItemLevel > Level ||
               armour.ArmourType == ArmourType.LEATHER ||
               armour.ArmourType == ArmourType.MAIL || armour.ArmourType == ArmourType.PLATE) {
                throw new InvalidArmorException();
            } else {
                CharactorInventory.AddItemInInventory(armour);
                result = "Success! armour equipped";
                UpdateTotalAttributes();
            }
            return result;
        }
        /// <summary>
        /// Equips a weapon to a character. 
        /// </summary>
        /// <param name="weapon">A weapon object.</param>
        /// <returns>Success or failure string message.</returns>
        public override string EquipWeaponToCharater(Weapon weapon) {
            string result = "";
           if((weapon.WeaponType == WeaponType.STAFFS || weapon.WeaponType == WeaponType.WANDS) && weapon.ItemLevel <= Level) {
                this.weapon = weapon;
                result = "Success! weapon equipped";
                CalculateDamage();
            } else {
                throw new InvalidWeaponException();
            }
            return result;
        }
        /// <summary>
        /// Levels up a character.
        /// </summary>
        /// <param name="level">Level number.</param>
        public override void LevelUp(int level) {
            if( level <= 0) {
                throw new ArgumentException("Not a valid level");
            } else {
                Level += level;
                this.PrimaryAttribute.Vitality += (level * 3);
                this.PrimaryAttribute.Strength += (level * 1);
                this.PrimaryAttribute.Dexterity += (level * 1);
                this.PrimaryAttribute.Intelligence += (level * 5);
                UpdateTotalAttributes();
            }   
        }
    }
}
